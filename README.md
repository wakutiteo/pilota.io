# Pilota.io

A clone of Agar.io written in Java with a Client-Server architecture.

Final work of 1st Multiplatform Application Development of the programming subject.


## Screenshots

![Playing Agar.io](/images/playing_background.png)
![Pilota.io main window](/images/pilotaio_main.png)
