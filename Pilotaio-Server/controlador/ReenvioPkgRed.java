import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.util.HashMap;

public class ReenvioPkgRed {
	public static final int PUERTO_ESCUCHA_CLI = 7575;
	

	/* Reenviar paquetes a todos los clientes conectados */
	public ReenvioPkgRed(HashMap<String, PkgCirculoComida> jugadores, Comida[] arrComida, PkgCirculoComida pkgJugador) {

		if (pkgJugador.isMeConecto()) {
			PkgCirculoComida pkgArrCompleto = new PkgCirculoComida(null);
			// Completo [2000]
			gestionarColisionComida(arrComida, jugadores);
			pkgArrCompleto.setArrComida(arrComida);
			jugadores.put(Servidor.STR_HM_COMIDA, pkgArrCompleto);
		} else {
			PkgCirculoComida pkgArrModificado = new PkgCirculoComida(null);
			pkgArrModificado.setModArrComida(gestionColComidaIndivual(arrComida, jugadores));
			jugadores.put(Servidor.STR_HM_COMIDA, pkgArrModificado);
		}

		String eliminarJugadorIp = detectarColisionesEntreJugadores(jugadores);
		if (eliminarJugadorIp != null) {
			jugadores.get(eliminarJugadorIp).setMorir(true);
		}

		// Reenviar paquete despues de las modificaciones pertinentes.
		reenviarPaquete(jugadores);

		// Eliminar del HashMap. Después de haber enviado la señal de morir al jugador.
		if (eliminarJugadorIp != null) {
			jugadores.remove(eliminarJugadorIp);
			// Mandar señal al resto de jugadores
			reenviarPaquete(jugadores);
		}
	}

	private void reenviarPaquete(HashMap<String, PkgCirculoComida> jugadores) {
		String ipActual = null;
		try {
			Socket sockEnvio;
			ObjectOutputStream pkgOutStrm;

			for (String ip : jugadores.keySet()) {
				if (ip.equals(Servidor.STR_HM_COMIDA)) {
					continue;
				}
				ipActual = ip; 

				sockEnvio = new Socket(ip, PUERTO_ESCUCHA_CLI);
				pkgOutStrm = new ObjectOutputStream(sockEnvio.getOutputStream());
 
				// Hacemos una copia porque si no modificamos el HashMap original.
				HashMap<String, PkgCirculoComida> jugadoresCopia = new HashMap<String, PkgCirculoComida>(jugadores);

				pkgOutStrm.writeObject(jugadoresCopia);

				// Aquí cerramos todo
				sockEnvio.close(); 
				pkgOutStrm.close();
			}
		} catch (ConnectException e) {
			jugadores.remove(ipActual);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private HashMap<Integer, Comida> gestionColComidaIndivual(Comida[] arrComida, HashMap<String, PkgCirculoComida> jugadores) {
		HashMap<Integer, Comida> modArrComida = new HashMap<Integer, Comida>();
		Comida puntoComida;
		PkgCirculoComida circ;
		double resultado;

		for (int i = 0; i < arrComida.length; i++) {
			for (String ip : jugadores.keySet()) {
				if (ip != Servidor.STR_HM_COMIDA) {

					puntoComida = arrComida[i];
					circ = jugadores.get(ip);
					resultado = calcularDistanciaCircComida(circ, puntoComida);

					// Colisión
					if (resultado < (puntoComida.getSize() / 2 + circ.getSize() / 2)) {
						Comida comidaNueva = new Comida();
						jugadores.get(ip).setSize(jugadores.get(ip).getSize() + puntoComida.getValor());
					
						modArrComida.put(i, comidaNueva);
						arrComida[i] = comidaNueva; 
					}
				}
			}
		}
		
		return modArrComida;
	}

	private double calcularDistanciaCircComida(PkgCirculoComida circ, Comida puntoComida) {
		double x1, y1, x2, y2;
		double xDist, yDist, resultado;

		// T. Pitagoras
		x1 = circ.getPosX() + circ.getSize() / 2;
		y1 = circ.getPosY() + circ.getSize() / 2;
		x2 = puntoComida.getPosX() + puntoComida.getSize() / 2;
		y2 = puntoComida.getPosY() + puntoComida.getSize() / 2;
		xDist = x2 - x1;
		yDist = y2 - y1;

		resultado = Math.sqrt(Math.pow(xDist, 2) + Math.pow(yDist, 2));
		return resultado;
	}


	private void gestionarColisionComida(Comida[] arrComida, HashMap<String, PkgCirculoComida> jugadores) {
		Comida puntoComida;
		PkgCirculoComida circ;
		double resultado;

		for (int i = 0; i < arrComida.length; i++) {
			for (String ip : jugadores.keySet()) {
				if (ip != Servidor.STR_HM_COMIDA) {
					// T. Pitagoras
					puntoComida = arrComida[i];
					circ = jugadores.get(ip);
					resultado = calcularDistanciaCircComida(circ, puntoComida);

					// Colisión
					if (resultado < (puntoComida.getSize() / 2 + circ.getSize() / 2)) {
						jugadores.get(ip).setSize(jugadores.get(ip).getSize() + puntoComida.getValor());
						arrComida[i] = new Comida(); 
					}
				}
			}
		}
	}


	private String detectarColisionesEntreJugadores(HashMap<String, PkgCirculoComida> jugadores) {
		int porcentCuerpoComido = 30; // 30%
		String ipMenorEliminar = null;
		PkgCirculoComida circ1, circ2;
		double x1, y1, x2, y2;
		double xDist, yDist, resultado;

		for (String ip : jugadores.keySet()) {
			for (String ip2 : jugadores.keySet()) {

				if (ip != ip2 && ip != Servidor.STR_HM_COMIDA && ip2 != Servidor.STR_HM_COMIDA) {
					// funcion hayColosion()
					// T. Pitagoras
					circ1 = jugadores.get(ip);
					circ2 = jugadores.get(ip2);
					x1 = circ1.getPosX() + circ1.getSize() / 2;
					y1 = circ1.getPosY() + circ1.getSize() / 2;
					x2 = circ2.getPosX() + circ2.getSize() / 2;
					y2 = circ2.getPosY() + circ2.getSize() / 2;
					xDist = x2 - x1;
					yDist = y2 - y1;
					resultado = Math.sqrt(Math.pow(xDist, 2) + Math.pow(yDist, 2));

					// Colisión
					if (resultado < (circ1.getSize() / 2 + circ2.getSize() / 2)) {
						PkgCirculoComida circuloMayor = getCirculoMayor(circ1, circ2);

						if (circuloMayor != null) {
							PkgCirculoComida circuloMenor = (circuloMayor == circ1 ? circ2 : circ1);

							int valorFinal = (circuloMenor.getSize() * porcentCuerpoComido / 100) + circuloMayor.getSize();
							circuloMayor.setSize(valorFinal);
							ipMenorEliminar = (circuloMenor == circ1 ? ip : ip2);
						}
					}
				}
			}
		}
		return ipMenorEliminar;
	}
	
	private PkgCirculoComida getCirculoMayor(PkgCirculoComida circ1, PkgCirculoComida circ2) {
		float porcentDiffSize = 1.15f; // 15%
		PkgCirculoComida circuloMayor = null;

		if (circ1.getSize() > circ2.getSize() * porcentDiffSize) {
			circuloMayor = circ1;
		} else if (circ2.getSize() > circ1.getSize() * porcentDiffSize) {
			circuloMayor = circ2;
		}
		return circuloMayor;
	}

}
