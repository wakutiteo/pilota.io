import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.HashMap;

public class GestionarJuego {

	private HashMap<String, PkgCirculoComida> jugadores;
	private Comida[] arrComida;
 	
	public GestionarJuego() {
		jugadores = new HashMap<String, PkgCirculoComida>();
		arrComida = new Comida[Servidor.NUM_COMIDA];
		generarComida();

		try {
			// TODO: Cerrar el socket al cerrar el programa?
			ServerSocket sockServer = new ServerSocket(Servidor.PUERTO); 

			PkgCirculoComida pkgCirculoRecibido;
			Socket sockEscucha;
			ObjectInputStream pkgInStrm;
			String ipCliente;
			

			while (true) {
				sockEscucha = sockServer.accept();

				pkgInStrm = new ObjectInputStream(sockEscucha.getInputStream());
				pkgCirculoRecibido = (PkgCirculoComida) pkgInStrm.readObject();

				ipCliente = sockEscucha.getRemoteSocketAddress().toString().split("/")[1].split(":")[0];
				if (gestionarNuevoCliente(pkgCirculoRecibido, ipCliente)) continue;
				if (gestionarDesconexionCliente(pkgCirculoRecibido, ipCliente)) continue;

				jugadores.replace(ipCliente, pkgCirculoRecibido);
				// Reenviar paquete broadcast. CONVERTIR A FUNCIÓN.
				ReenvioPkgRed reenvioPkgRed = new ReenvioPkgRed(jugadores, arrComida, pkgCirculoRecibido);


				// Cerramos todo:
				sockEscucha.close();
				pkgInStrm.close();
			}

		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		
	}


	private void generarComida() {
		for (int i = 0; i < arrComida.length; i++) {
			arrComida[i] = new Comida();
		}
	}

	private boolean gestionarNuevoCliente(PkgCirculoComida pkgCirculoRecibido, String ipCliente) {
		boolean esNuevo = false;

		// Si el cliente se conecta
		if (pkgCirculoRecibido.isMeConecto()) {
			jugadores.put(ipCliente, pkgCirculoRecibido);
			// Mandamos conexión a los clientes. Con el circulo completo que nos ha enviado el cliente conectado.
			ReenvioPkgRed reenvioPkgRed = new ReenvioPkgRed(jugadores, arrComida, pkgCirculoRecibido);
			esNuevo = true;
		}
		return esNuevo;
	}

	private boolean gestionarDesconexionCliente(PkgCirculoComida pkgCirculoRecibido, String ipCliente) {
		boolean seDesconecta = false;
		// Si el cliente se desconecta
		if (pkgCirculoRecibido.isMeDesconecto()) {
			// Hacer lo pertinente al desconectar: sacar ip del ranking
			// Dejar de enviar a los demás usuario. Mandarles que ya no está.
			// Cerrar la conexión con el...
			jugadores.remove(ipCliente);
			// Mandamos desconexión a los clientes.
			ReenvioPkgRed reenvioPkgRed = new ReenvioPkgRed(jugadores, arrComida, pkgCirculoRecibido);
			seDesconecta = true;
		}
		return seDesconecta;
	}

}
