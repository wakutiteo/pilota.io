import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public class GestionRed {
	public static final int PUERTO_ESCUCHA_CLI = 7575;
	
	private Pilotaio pilotaio;
	public GestionRed(Pilotaio pilotaio) {
		this.pilotaio = pilotaio;
	}
	
	
	// Métodos
	public void recibirPkgRed(CanvasJuego canvasJuego, Circulo circulo, Thread hiloEscucha, PanelCanvasJuego panelCanvasJuego) {
		try {
			ServerSocket servSockEscuchaCli = new ServerSocket(PUERTO_ESCUCHA_CLI);
			HashMap<String, PkgCirculoComida> pkgJugadoresRecibidos;

			while (true) {
				// Creamos le Socket por el cual va a recibir el paquete.
				Socket sockRecbCli = servSockEscuchaCli.accept();
						
				ObjectInputStream pkgInStrm = new ObjectInputStream(sockRecbCli.getInputStream());
				pkgJugadoresRecibidos = (HashMap<String, PkgCirculoComida>) pkgInStrm.readObject();

				// Actualizamos comida
				if (pkgJugadoresRecibidos.get(Pilotaio.STR_HM_COMIDA).getModArrComida() != null) {
					actualizarComida(pkgJugadoresRecibidos.get(Pilotaio.STR_HM_COMIDA), canvasJuego);
					pkgJugadoresRecibidos.remove(Pilotaio.STR_HM_COMIDA);
				} else {
					canvasJuego.setArrComida(pkgJugadoresRecibidos.get(Pilotaio.STR_HM_COMIDA).getArrComida());
					pkgJugadoresRecibidos.remove(Pilotaio.STR_HM_COMIDA);
				}
 

				// Si no está en jugador mismo Pkg significa que ha muerto.
				String ipJugador = sockRecbCli.getLocalAddress().getHostAddress();
				if (pkgJugadoresRecibidos.get(ipJugador).isMorir()) {
					try {
						// Funcionando!!
						hiloEscucha.join(20);
						// Panel
						PanelInicio panelInicio = new PanelInicio(pilotaio);
						pilotaio.getContentPane().remove(panelCanvasJuego);
						pilotaio.getContentPane().add(panelInicio);
						pilotaio.repaint();
						panelInicio.msgMuerte(circulo.getSize());

						// Cerramos todo
						servSockEscuchaCli.close();
						sockRecbCli.close();
						pkgInStrm.close();

						break;
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
				circulo.setSize(pkgJugadoresRecibidos.get(ipJugador).getSize());
				
				
				// Eliminamos del array al jugador
				pkgJugadoresRecibidos.remove(ipJugador);
				// Asignamos sin el jugador
				canvasJuego.setJugadoresPkgRecibidos(pkgJugadoresRecibidos);
			}

		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
 
	private void actualizarComida(PkgCirculoComida pkgComida, CanvasJuego canvasJuego) {
		for (int indiceArr : pkgComida.getModArrComida().keySet()) {
			// Asignamos en el indice que corespondiente la Comida correspondiente.
			canvasJuego.getArrComida()[indiceArr] = pkgComida.getModArrComida().get(indiceArr);
		}
	}


	public void envioPkgRed(Circulo circulo, Pilotaio pilotaio, CanvasJuego canvasJuego, boolean buclePintar) {
		/* Conexión con el servidor */
		try {
			Socket sockEnvio = new Socket(PanelCanvasJuego.IP_SERV, PanelCanvasJuego.PUERTO_SERV);

			PkgCirculoComida pkgCirculo = new PkgCirculoComida(
					circulo.getPosX(), circulo.getPosY(), 
					circulo.getNombre(), circulo.getColor(),
					circulo.getColorBorde(), circulo.getVelocidad(), 
					circulo.getSize() 
			);
			
			ObjectOutputStream pkgOutStrm = new ObjectOutputStream(sockEnvio.getOutputStream());
			pkgOutStrm.writeObject(pkgCirculo);
			
			// Aquí cerramos todo
			sockEnvio.close();
			pkgOutStrm.close();
			
		} catch (ConnectException e1) {
			canvasJuego.setBuclePintar(false);
			pilotaio.dialogoErr(pilotaio.getContentPane(), "Se ha perdido la conexión con el servidor.");
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	

}
