import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EventosPanelInicio {

	private PanelInicio panelInicio;
	private Pilotaio pilotaio;

	public EventosPanelInicio(Pilotaio pilotaio, PanelInicio panelInicio) {
		this.panelInicio = panelInicio;
		this.pilotaio = pilotaio;

		panelInicio.getBtnPlay().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				panelJugar();
			}
		});
		panelInicio.getTxtNickname().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				panelJugar();
			}
		});

	}

	
	// Métodos
	private void panelJugar() {
		PanelCanvasJuego panelCanvasJuego = new PanelCanvasJuego(pilotaio, panelInicio.getTxtNickname().getText());
		pilotaio.getContentPane().remove(panelInicio);
		pilotaio.getContentPane().add(panelCanvasJuego);
		pilotaio.repaint();
	}

}
