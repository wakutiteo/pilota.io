import java.awt.Color;
import java.awt.Cursor;
import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class PanelInicio extends JPanel {

	private JTextFieldRedondeado txtNickname;
	private JButton btnPlay;
	private JLabel lblLogo;
	private Image imgTitulo;
	private EventosPanelInicio eventosPanelInicio;
	private Pilotaio pilotaio;
	private JLabel lblMuerte;
	private JLabel lblMuertePuntos;

	public PanelInicio(Pilotaio pilotaio) {
		this.pilotaio = pilotaio;

		setBackground(Pilotaio.COLOR_FONDO);
		setSize(Pilotaio.ANCHO, Pilotaio.ALTO);
		setLayout(null);

		btnPlay = new JButton("Play");
		btnPlay.setFont(Pilotaio.FUENTE);
		btnPlay.setForeground(Color.WHITE);
		btnPlay.setBackground(Color.decode("#67ca98"));
		btnPlay.setBounds(500, 378, 93, 50);
		btnPlay.setUI(new StyledButtonUI(50));
		Cursor cursorMano = new Cursor(Cursor.HAND_CURSOR);
		btnPlay.setCursor(cursorMano);
		add(btnPlay);

		txtNickname = new JTextFieldRedondeado(new Color(76, 68, 124));
		txtNickname.setDocument(new JTextFieldLimiteCrt(14));
		txtNickname.setFont(Pilotaio.FUENTE);
		txtNickname.setForeground(Color.decode("#e0e0ff"));
		txtNickname.setBackground(new Color(76, 68, 124));
		txtNickname.setBounds(439, 318, 223, 41);
		//txtNickname.setBorder(BorderFactory.createEmptyBorder(0, 26, 0, 26));
		txtNickname.setColumns(10);
		TextPrompt placeholder = new TextPrompt("Nickname", txtNickname);
		placeholder.setHorizontalAlignment(JTextField.CENTER);
		placeholder.changeAlpha(0.6f);
		add(txtNickname);

		lblMuerte = new JLabel();
		lblMuerte.setFont(Pilotaio.FUENTE_MUERTE);
		lblMuerte.setBounds(473, 267, 131, 15);
		lblMuerte.setForeground(new Color(1, 1, 1, 0.45f));
		add(lblMuerte);

		lblMuertePuntos = new JLabel();
		lblMuertePuntos.setFont(Pilotaio.FUENTE_RANKING);
		lblMuertePuntos.setForeground(new Color(255, 255, 255));
		lblMuertePuntos.setBounds(601, 267, 35, 15);
		add(lblMuertePuntos);
		
		
		lblLogo = new JLabel();
		lblLogo.setBounds(350, 121, 400, 105);
		try {
			imgTitulo = ImageIO.read(getClass().getResource("/images/pilotaio-logo.png"))
					.getScaledInstance(lblLogo.getWidth(), lblLogo.getHeight(), Image.SCALE_SMOOTH);
		} catch (IOException e) {
			e.printStackTrace();
		}
		lblLogo.setIcon(new ImageIcon(imgTitulo));
		add(lblLogo);

		// Centrar elementos
		lblLogo.setLocation(((this.getWidth() / 2) - (lblLogo.getWidth() / 2)), lblLogo.getY());
		txtNickname.setLocation(((this.getWidth() / 2) - (txtNickname.getWidth() / 2)), txtNickname.getY());
		btnPlay.setLocation(((this.getWidth() / 2) - (btnPlay.getWidth() / 2)), btnPlay.getY());


		eventosPanelInicio = new EventosPanelInicio(pilotaio, this);
	}
	
	

	// Getters & Setters
	public JButton getBtnPlay() {
		return btnPlay;
	}

	public void setBtnPlay(JButton btnPlay) {
		this.btnPlay = btnPlay;
	}
	
	public JTextFieldRedondeado getTxtNickname() {
		return txtNickname;
	}

	public void setTxtNickname(JTextFieldRedondeado txtNickname) {
		this.txtNickname = txtNickname;
	}



	// Métodos
	public void msgMuerte(int size) {
		lblMuerte.setText("Tu tamaño final fue de" );
		lblMuertePuntos.setText(String.valueOf(size));
	}
	/* public void msgMuerteRm(int size) {
		lblMuerte.setText(null);
		lblMuertePuntos.setText(null);
	} */
}
