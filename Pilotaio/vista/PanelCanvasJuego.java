import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;

import javax.swing.JPanel;

public class PanelCanvasJuego extends JPanel {
	public static final String IP_SERV = "192.168.1.68";
	public static final int PUERTO_SERV = 7474;

	private CanvasJuego canvasJuego;
	private Pilotaio pilotaio;

	public PanelCanvasJuego(Pilotaio pilotaio, String nombreJugador) {
		this.pilotaio = pilotaio;

		setLayout(null);
		setSize(Pilotaio.ANCHO, Pilotaio.ALTO);

		canvasJuego = new CanvasJuego(pilotaio, nombreJugador, this);
		canvasJuego.setLocation(0, 0);
		add(canvasJuego);

		// Al cerrar la ventana mandamos señal de desconexión.
		pilotaio.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				try {
					// Desconectar
					Socket sockEnvio = new Socket(IP_SERV, PUERTO_SERV);
					PkgCirculoComida pkgCirculo = new PkgCirculoComida(false, true);

					ObjectOutputStream pkgOutStrm = new ObjectOutputStream(sockEnvio.getOutputStream());
					pkgOutStrm.writeObject(pkgCirculo);

					// Aquí cerramos todo
					sockEnvio.close();
					pkgOutStrm.close();
				} catch (ConnectException e1) {
					pilotaio.dialogoErr(PanelCanvasJuego.this, "No hay conexión con el servidor.");
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
	}

}
