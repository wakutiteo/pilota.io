import java.awt.Canvas;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.NoRouteToHostException;
import java.net.Socket;
import java.util.HashMap;

public class CanvasJuego extends Canvas implements Runnable {
	public static final int ANCHO = Pilotaio.ANCHO;
	public static int ALTO;
	public static final int ANCHO_MAPA = 4000;
	public static final int ALTO_MAPA = 3000;

	private Pilotaio pilotaio;
	private PanelCanvasJuego panelCanvasJuego;
	// Doble buffer
	private Image buffer;
	private Graphics pantVirtual;
	private Circulo circulo;
	private Camara camara;

	private double ratonX, ratonY;

	private HashMap<String, PkgCirculoComida> jugadoresPkgRecibidos;
	private HashMap<String, Circulo> jugadoresCirculo;

	private Ranking ranking;
	private MiRanking miRanking;
	private Comida[] arrComida;
	private GestionRed gestionRed;
	private Thread hiloEscucha;
	private boolean buclePintar;

	public CanvasJuego(Pilotaio pilotaio, String nombreJugador, PanelCanvasJuego panelCanvasJuego) {
		setSize(Pilotaio.ANCHO, (pilotaio.ALTO - pilotaio.getInsets().top));
		ALTO = (int) getSize().getHeight();

		this.pilotaio = pilotaio;
		this.panelCanvasJuego = panelCanvasJuego;
		gestionRed = new GestionRed(pilotaio);
		setBackground(Pilotaio.COLOR_FONDO);

		arrComida = new Comida[Pilotaio.NUM_COMIDA];
		jugadoresCirculo = new HashMap<String, Circulo>();
		circulo = new Circulo(3200, 2800, nombreJugador);
		camara = new Camara(circulo);
		ranking = new Ranking(jugadoresCirculo, circulo);
		miRanking = new MiRanking(circulo);

		// Posicionamos la camara.
		camara.tick();

		// Iniciar hilo de escucha antes de conectarnos.
		hiloEscucha = new Thread(this);
		hiloEscucha.start();

		// Mandamos señal de conexión al servidor
		conectarJugador();

		// Pintamos
		tick();
	}

	// Getters & Setters
	public HashMap<String, PkgCirculoComida> getJugadoresPkgRecibidos() {
		return jugadoresPkgRecibidos;
	}

	public void setJugadoresPkgRecibidos(HashMap<String, PkgCirculoComida> jugadoresRecibidos) {
		this.jugadoresPkgRecibidos = jugadoresRecibidos;

		gestionarJugadoresCirculo(jugadoresRecibidos);
	}

	private void gestionarJugadoresCirculo(HashMap<String, PkgCirculoComida> jugadoresRecibidos) {
		for (String ip : jugadoresCirculo.keySet()) {
			// Si un jugador que estaba, no está en el nuevo paquete.
			// Significa que ha MUERTO.
			if (jugadoresRecibidos.get(ip) == null) {
				System.out.println("Eliminando...");
				jugadoresCirculo.remove(ip);
				break;
			}
		}

		for (String ip : jugadoresRecibidos.keySet()) {
			// Si existe, ACTUALIZAR datos.
			if (jugadoresCirculo.get(ip) != null) {
				Circulo circuloActual = new HashMap<String, Circulo>(jugadoresCirculo).get(ip);
				PkgCirculoComida circuloRecibidoActual = new HashMap<String, PkgCirculoComida>(jugadoresRecibidos)
						.get(ip);

				// Por el momento, esto es lo único que puede cambiar.
				circuloActual.setPosX(circuloRecibidoActual.getPosX());
				circuloActual.setPosY(circuloRecibidoActual.getPosY());
				circuloActual.setVelocidad(circuloRecibidoActual.getVelocidad());
				circuloActual.setSize(circuloRecibidoActual.getSize());

			} else {
				// Si no exite (se conecta jugador nuevo). AÑADIR.
				jugadoresCirculo.put(ip, new Circulo(jugadoresRecibidos.get(ip)));
			}
		}
	}

	public Comida[] getArrComida() {
		return arrComida;
	}

	public void setArrComida(Comida[] arrComida) {
		this.arrComida = arrComida;
	}

	public boolean isBuclePintar() {
		return buclePintar;
	}

	public void setBuclePintar(boolean buclePintar) {
		this.buclePintar = buclePintar;
	}

	// Métodos
	private void conectarJugador() {
		try {
			// Conectar
			Socket sockEnvio = new Socket(PanelCanvasJuego.IP_SERV, PanelCanvasJuego.PUERTO_SERV);
			PkgCirculoComida pkgCirculo = new PkgCirculoComida(circulo.getPosX(), circulo.getPosY(),
					circulo.getNombre(), circulo.getColor(), circulo.getColorBorde(), circulo.getVelocidad(),
					circulo.getSize());
			pkgCirculo.setMeConecto(true);

			ObjectOutputStream pkgOutStrm = new ObjectOutputStream(sockEnvio.getOutputStream());
			pkgOutStrm.writeObject(pkgCirculo);

			// Aquí cerramos todo
			sockEnvio.close();
			pkgOutStrm.close();
		} catch (ConnectException | NoRouteToHostException e1) {
			pilotaio.dialogoErr(pilotaio.getContentPane(), "No hay conexión con el servidor.");
			System.exit(0);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

	private void tick() {
		buclePintar = true;
		Thread hiloPintar = new Thread(new Runnable() {
			public void run() {
				while (buclePintar) {
					repaint();
					try {
						Thread.sleep(10); 
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		hiloPintar.start();
	}

	@Override
	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		configurarG2(g2);

		// Pricipio de la camara
		g2.translate(camara.getPosX(), camara.getPosY());

		// Dibujamos todos
		dibujarFondoGrid(g2);
		dibujarComida(g2);
		circulo.dibujar(g2); 
		dibujarJugadoresCirc(g2);

		g2.translate(-camara.getPosX(), -camara.getPosY());
		ranking.dibujar(g2);
		miRanking.dibujar(g2);
		//g2.translate(camara.getPosX(), camara.getPosY());


		if (getMousePosition() == null) return;

		posicionarCirculo();
		camara.tick();

		gestionRed.envioPkgRed(circulo, pilotaio, this, buclePintar);

		// Final de la camara
		g2.translate(-camara.getPosX(), -camara.getPosY());
	}

	private void dibujarComida(Graphics2D g2) {
		if (arrComida != null && arrComida[0] != null) {
			for (int i = 0; i < arrComida.length; i++) {
				arrComida[i].dibujar(g2);
			} 

		}
	}

	private void posicionarCirculo() {
		try {
			// La posición del ratón siempre: 0-1100
			ratonX = getMousePosition().getX() + (-camara.getPosX());
			ratonY = getMousePosition().getY() + (-camara.getPosY());
		} catch (NullPointerException e) {
			// Nada... todo bien... solo ha salido el ratón de la ventana.
		}

		// Cursor está dentro del circulo
		if (ratonX > circulo.getPosX() && ratonX < circulo.getPosX() + circulo.getSize() && ratonY > circulo.getPosY()
				&& ratonY < circulo.getPosY() + circulo.getSize()

		) {
			return;
		}

		// Distancia en X,Y desde el círculo a el ratón.
		double distanciaX = ratonX - circulo.getPosX() - circulo.getSize() / 2;
		double distanciaY = ratonY - circulo.getPosY() - circulo.getSize() / 2;

		// Obtenemos el ángulo después de convertir coordenadas rectangulares a
		// coordenadas polares.
		double angulo = Math.atan2(distanciaY, distanciaX);

		// Posición de Circulo + (Cos(angulo) * circulo.velocidad)
		double pasoX = circulo.getVelocidad() * Math.cos(angulo);
		double pasoY = circulo.getVelocidad() * Math.sin(angulo);

		circulo.setPosX(circulo.getPosX() + pasoX);
		circulo.setPosY(circulo.getPosY() + pasoY);
	}

	private void configurarG2(Graphics2D g2) {
		// Anti-aliasing
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		// Fuente
		g2.setFont(Pilotaio.FUENTE);
	}

	private void dibujarJugadoresCirc(Graphics2D g2) {
		if (jugadoresCirculo != null && !jugadoresCirculo.isEmpty()) {
			FontMetrics fm = g2.getFontMetrics();

			/*g2.translate(
				camara.getPosX() -ANCHO/2 + fm.stringWidth(circulo.getNombre())/2,
				camara.getPosY() -ALTO/2 - circulo.getGrosorBorde()/2 
			);*/
			for (String ip : jugadoresCirculo.keySet()) {

				//System.out.println(jugadoresCirculo.get(ip).getNombre());
				jugadoresCirculo.get(ip).dibujar(g2);
			}
		}
	}

	private void dibujarFondoGrid(Graphics2D g2) {
		g2.setColor(Color.decode("#B695C0").darker().darker());

		for (int i = 0; i < ANCHO_MAPA || i < ALTO_MAPA; i += 35) {
			// Vertical
			if (i < ANCHO_MAPA) {
				g2.drawLine(i, 0, i, ALTO_MAPA);
			}
			// Horizontal
			if (i < ALTO_MAPA) {
				g2.drawLine(0, i, ANCHO_MAPA, i);
			}
		}
	}

	/* Doble buffer */
	@Override
	public void update(Graphics g) {
		buffer = createImage(this.getWidth(), this.getHeight());
		pantVirtual = buffer.getGraphics();
		paint(pantVirtual);
		g.drawImage(buffer, 0, 0, getWidth(), getHeight(), this);
	}

	// static ya que el circulo no debería necesitar una instancia de
	// esta clase.
	public static Color oscurecerColor(Color color, int cantidad) {
		int rojo, verde, azul;

		rojo = (color.getRed() * cantidad) / 100;
		verde = (color.getGreen() * cantidad) / 100;
		azul = (color.getBlue() * cantidad) / 100;

		Color colorOscurecido = new Color(color.getRed() - rojo, color.getGreen() - verde, color.getBlue() - azul);

		return colorOscurecido;
	}


	/* Hilo de escucha */
	@Override
	public void run() {
		gestionRed.recibirPkgRed(this, circulo, hiloEscucha, panelCanvasJuego);
	} 
}
