import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

public class Pilotaio extends JFrame {
	public static final int ANCHO = 1100;
	public static final int ALTO = 700;
	public static final Font FUENTE = new Font("Noto Sans", Font.BOLD, 16);
	public static final Font FUENTE_RANKING = new Font("Noto Sans", Font.BOLD, 12);
	public static final Font FUENTE_MUERTE = new Font("Noto Sans", Font.PLAIN, 12);
	public static final Color COLOR_FONDO = Color.decode("#161c22");
	public static final Color COLOR_BTN_TEXTO = Color.decode("#e0e0ff");
	public static final int NUM_COMIDA = 500;
	public static final String STR_HM_COMIDA = "comida";

	private JPanel contentPane;
	private PanelInicio panelInicio;


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pilotaio frame = new Pilotaio(); 
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	public Pilotaio() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, ANCHO, ALTO);
		contentPane = new JPanel();
		//contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		
		setTitle("Pilotaio");
		setResizable(false);
		try {
			setIconImage(ImageIO.read(getClass().getResource("/images/icon.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		panelInicio = new PanelInicio(this);
		contentPane.add(panelInicio);
	}

	
	
	// Métodos
	public void dialogoErr(Component componente, String msg) {
		
		UIManager UI = new UIManager();
		UI.put("OptionPane.background", Pilotaio.COLOR_FONDO);
		UI.put("Panel.background", Pilotaio.COLOR_FONDO);
		UI.put("OptionPane.messageForeground", Pilotaio.COLOR_BTN_TEXTO);


		ImageIcon icon = new ImageIcon(getClass().getResource("/images/icon.png"));

		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setUI(new StyledButtonUI(30));

		btnAceptar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});

		JOptionPane.showOptionDialog(
			     componente, 
			     msg,
			     "Dialogo de error", 
			     JOptionPane.YES_NO_OPTION, 
			     JOptionPane.ERROR_MESSAGE, 
			     icon, 
			     new Object[]{btnAceptar}, 
			     btnAceptar
		);
	}
}
