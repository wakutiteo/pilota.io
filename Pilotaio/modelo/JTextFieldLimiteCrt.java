import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class JTextFieldLimiteCrt extends PlainDocument {

	private int limite;
	
	public JTextFieldLimiteCrt(int limite) {
		this.limite = limite;
	}
	
	public void insertString(int offset, String str, AttributeSet set) throws BadLocationException {
		if (str == null) {
			return;
		} else if ((getLength() + str.length()) <= limite) {
			super.insertString(offset, str, set);
		}
	}
}
