import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.basic.BasicButtonUI;

class StyledButtonUI extends BasicButtonUI {
	
	private int cantidadRedondeo;
	
	public StyledButtonUI(int cantidadRedondeo) {
		this.cantidadRedondeo = cantidadRedondeo;
	}
	

    @Override
    public void installUI (JComponent c) {
        super.installUI(c);
        AbstractButton button = (AbstractButton) c;
        button.setOpaque(false);
        button.setBorder(new EmptyBorder(5, 15, 5, 15));
        button.setForeground(Pilotaio.COLOR_BTN_TEXTO);
    }

    @Override
    public void paint (Graphics g, JComponent c) {
        AbstractButton b = (AbstractButton) c;
        paintBackground(g, b, b.getModel().isPressed() ? 2 : 0);
        super.paint(g, c);
    }

    private void paintBackground (Graphics g, JComponent c, int yOffset) {
        Dimension size = c.getSize();
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setPaint(new GradientPaint(
					new Point(0, 0), 
					Color.decode("#56ac81"), 
					new Point(0, size.height), 
					Color.decode("#387054")
        		)
        );
        g.fillRoundRect(0, yOffset, size.width, size.height - yOffset, cantidadRedondeo, cantidadRedondeo);
        
        // dispose provoca que el texto del boton no se vea.
        //g2.dispose();
    }
}