

public class Camara {
	
	private double posX;
	private double posY;
	
	private Circulo circulo;
	

	public Camara(Circulo circulo) {
		//this.posX = posX;
		//this.posY = posY;
		this.circulo = circulo;
	}
	
	
	// Getters & Setters
	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}


	// Métodos
	public void tick() {
		posX = -circulo.getPosX() + CanvasJuego.ANCHO/2 - circulo.getSize()/2;
		posY = -circulo.getPosY() + CanvasJuego.ALTO/2 - circulo.getSize()/2;
	}
}
