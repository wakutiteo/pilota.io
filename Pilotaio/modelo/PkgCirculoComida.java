import java.awt.Color;
import java.io.Serializable;
import java.util.HashMap;

public class PkgCirculoComida implements Serializable {
	
	private double posX, posY;
	private String nombre;
	private Color color, colorBorde;
	private double velocidad;
	private int size;
	private boolean meConecto, meDesconecto;
	private boolean morir;
	private Comida[] arrComida;
	private HashMap<Integer, Comida> modArrComida;

	public PkgCirculoComida(double posX, double posY, String nombre, Color color, Color colorBorde, 
			double velocidad, int size) {
		this.posX = posX; 
		this.posY = posY;
		this.nombre = nombre;
		this.color = color;
		this.colorBorde = colorBorde;
		this.velocidad = velocidad;
		this.size = size;
		meConecto = false;
		meDesconecto = false;
	}

	public PkgCirculoComida(Comida[] arrComida) {
		this.arrComida = arrComida;
	}
	
	public PkgCirculoComida(boolean meConecto, boolean meDesconecto) {
		this.meConecto = meConecto;
		this.meDesconecto = meDesconecto;
	}

	// Getters & Setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Color getColorBorde() {
		return colorBorde;
	}

	public void setColorBorde(Color colorBorde) {
		this.colorBorde = colorBorde;
	}

	public double getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(double velocidad) {
		this.velocidad = velocidad;
	}

	public boolean isMeDesconecto() {
		return meDesconecto;
	}

	public void setMeDesconecto(boolean meDesconecto) {
		this.meDesconecto = meDesconecto;
	}

	public boolean isMeConecto() {
		return meConecto;
	}

	public void setMeConecto(boolean meConecto) {
		this.meConecto = meConecto;
	}

	public boolean isMorir() {
		return morir;
	}

	public void setMorir(boolean morir) {
		this.morir = morir;
	}

	public void setArrComida(Comida[] arrComida) {
		this.arrComida = arrComida;
	}

	public Comida[] getArrComida() {
		return arrComida;
	}

	public HashMap<Integer, Comida> getModArrComida() {
		return modArrComida;
	}

	public void setModArrComida(HashMap<Integer, Comida> modArrComida) {
		this.modArrComida = modArrComida;
	}
}
