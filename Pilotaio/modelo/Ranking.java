import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.font.TextLayout;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

public class Ranking {
	private final int MARGEN_H_TEXTO = 10;
	private final int MARGEN_V_TEXTO = 5;

	private double posX, posY;
	private int ancho, alto;
	private int numJugadoresRanking;
	private Circulo[] circuloJugadoresRanking;
	
	private HashMap<String, Circulo> jugadoresMap;
	private Circulo circulo;


	public Ranking(HashMap<String, Circulo> jugadoresMap, Circulo circulo) {
		ancho = 180;
		alto = 240;
		numJugadoresRanking = 10;
		posX = CanvasJuego.ANCHO - ancho - 10; 
		posY = 5;

		this.jugadoresMap = jugadoresMap;
		this.circulo = circulo;

		obtenerJugadoresRanking(ordenarRankingJugadores(jugadoresMap));
	}

	private void obtenerJugadoresRanking(List<Entry<String, Circulo>> ordenarRankingJugadores) {
		Circulo[] circulosRanking = new Circulo[numJugadoresRanking];

		for (int i = 0; i < circulosRanking.length && ordenarRankingJugadores.size() > i; i++) {

			circulosRanking[i] = ordenarRankingJugadores.get(i).getValue();
		}
		circuloJugadoresRanking = circulosRanking;
	}

	public void dibujar(Graphics2D g2) {
		String titulo = "Leaderboard";
		FontMetrics fm = g2.getFontMetrics();
		int posH = MARGEN_H_TEXTO;
		int posV = MARGEN_V_TEXTO + fm.getHeight(); 

		// Actualizamos el Ranking
		obtenerJugadoresRanking(ordenarRankingJugadores(jugadoresMap));


		// Dibujamos
		g2.setColor(new Color(0, 0, 0, 0.1f));
		g2.fillRect((int) posX, (int)posY, ancho, alto);

		g2.setColor(Pilotaio.COLOR_BTN_TEXTO);
		int posTituloX = (int) (posX + fm.stringWidth(titulo)/2 - posH); 
		g2.drawString(
				titulo,
				posTituloX,
				(int) (posY + posV)
		);
		posV += MARGEN_V_TEXTO + fm.getHeight() + 5;


		g2.setFont(Pilotaio.FUENTE_RANKING);
		for (int i = 0; i < circuloJugadoresRanking.length && circuloJugadoresRanking[i] != null; i++) {
			g2.setColor(circuloJugadoresRanking[i].getColor());
			g2.drawString(
					"#"+ (i+1) + "   " + circuloJugadoresRanking[i].getNombre(),
					(int) (posX + posH), 
					(int) (posY + posV)
			);
			g2.drawString(
					String.valueOf(circuloJugadoresRanking[i].getSize()),
					(int) (posX + ancho - (fm.stringWidth(String.valueOf(circuloJugadoresRanking[i].getSize()))) - MARGEN_H_TEXTO), 
					(int) (posY + posV)
			);

			posV += MARGEN_V_TEXTO + fm.getHeight();
		} 
		g2.setFont(Pilotaio.FUENTE);
	}


	private List<Entry<String, Circulo>> ordenarRankingJugadores(HashMap<String, Circulo> jugadoresMap) {
		List<Entry<String, Circulo>> listaOrdenada;
		HashMap<String, Circulo> jugadoresMapCopia = new HashMap<String, Circulo>(jugadoresMap);
		jugadoresMapCopia.put("yo", circulo);

		listaOrdenada = new LinkedList<Entry<String,Circulo>>(jugadoresMapCopia.entrySet());
		
		Collections.sort(listaOrdenada, new Comparator<Entry<String, Circulo>>() {

			@Override
			public int compare(Entry<String, Circulo> o1, Entry<String, Circulo> o2) {
				return o2.getValue().getSize() - o1.getValue().getSize();
			}
		});
		return listaOrdenada;
	}

}
