import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.io.Serializable;

public class Comida implements Serializable {
	
	private double posX, posY;
	private int valor;
	private Color color;
	private int size;
	
	public Comida() {
		valor = 1;
		size = 8 + valor;
		posX = Math.random() * (CanvasJuego.ANCHO_MAPA - size);
		posY = Math.random() * (CanvasJuego.ALTO_MAPA - size);
		color = establecerColor();
	}

	

	// Getters & Setters
	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	
	
	// Métodos
	private Color establecerColor() {
		Color color;
		int rojo, verde, azul;

		rojo = (int) (Math.random() * 176 + 70);
		verde = (int) (Math.random() * 176 + 70); 
		azul = (int) (Math.random() * 176 + 70); 
		
		color = new Color(rojo, verde, azul);
		return color;
	}

	public void dibujar(Graphics2D g2) {
		// Circulo 
		g2.setColor(color);
		g2.fillOval((int) posX, (int) posY, size, size);
	}
}
