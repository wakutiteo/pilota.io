import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.font.FontRenderContext; import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;

public class Circulo {
	
	private double posX, posY;
	private Color color, colorBorde;
	private String nombre;
	private double velocidad;
	private int size;
	private int grosorBorde = 5; 
	
	public Circulo(double posX, double posY, String nombre) {
		this.nombre = nombre; 
		if (nombre.isEmpty()) {
			this.nombre = "dot";
		}
		this.posX = posX;
		this.posY = posY;
		velocidad = 2.4;
		size = 40;
		color = establecerColor();
		colorBorde = CanvasJuego.oscurecerColor(color, 20);
	}
	
	public Circulo(PkgCirculoComida pkgCirculo) {
		this.posX = pkgCirculo.getPosX();
		this.posY = pkgCirculo.getPosY();
		this.color = pkgCirculo.getColor();
		this.colorBorde = pkgCirculo.getColorBorde();
		this.nombre = pkgCirculo.getNombre();
		this.velocidad = pkgCirculo.getVelocidad();
		// ??
		this.size = pkgCirculo.getSize();
	}




	// Getters & Setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		if (posX > CanvasJuego.ANCHO_MAPA - size) {
			posX = CanvasJuego.ANCHO_MAPA - size;
		} else if (posX < 0) {
			posX = 0;
		}
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		if (posY > (CanvasJuego.ALTO_MAPA - size)) {
			posY = CanvasJuego.ALTO_MAPA - size;
		} else if (posY < 0) {
			posY = 0;
		}
		this.posY = posY;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		//this.size = size;
		animarCrecimiento(size);
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Color getColorBorde() {
		return colorBorde;
	}

	public void setColorBorde(Color colorBorde) {
		this.colorBorde = colorBorde;
	}

	public double getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(double velocidad) {
		this.velocidad = velocidad;
	}

	public int getGrosorBorde() {
		return grosorBorde;
	}

	public void setGrosorBorde(int grosorBorde) {
		this.grosorBorde = grosorBorde;
	}



	// Métodos
	private Color establecerColor() {
		Color color;
		int rojo, verde, azul;

		rojo = (int) (Math.random() * 176 + 70);
		verde = (int) (Math.random() * 176 + 70); 
		azul = (int) (Math.random() * 176 + 70); 
		
		color = new Color(rojo, verde, azul);
		return color;
	}

	
	public void dibujar(Graphics2D g2) {
		FontMetrics fm = g2.getFontMetrics();
		// Circulo
		g2.setColor(color);
		g2.fillOval((int) posX, (int) posY, size, size);
		// Borde
		g2.setColor(colorBorde);
		g2.setStroke(new BasicStroke(grosorBorde));
		g2.drawOval((int) posX, (int) posY,  size, size);

		// Nombre
		g2.setColor(Color.black);
		int posFX = (int) (posX + (size/2) - (fm.stringWidth(nombre) / 2)); 
		int posFY = (int) (posY + (size/2) + (fm.getHeight() / 4)); 
		
        // Get the current transform
        AffineTransform saveAT = g2.getTransform();
        AffineTransform transform = g2.getTransform();

        transform.translate(posFX - transform.getTranslateX(), posFY - transform.getTranslateY());
        // Perform transformation
        g2.transform(transform);
        // Render
        g2.setColor(Color.BLACK);
        TextLayout tl = new TextLayout(nombre, g2.getFont(), g2.getFontRenderContext());
        Shape shape = tl.getOutline(null);
        g2.setStroke(new BasicStroke(2f));
        g2.draw(shape);
        g2.setColor(Color.WHITE);
        g2.fill(shape);
        
        // Restore original transform
        g2.setTransform(saveAT);
	}
	
	private void animarCrecimiento(int sizeFinal) {
		Thread hilo = new Thread(new Runnable() {
			public void run() {

				while (size < sizeFinal) {
					try {
						size += 1;
						setPosX(getPosX() - 1);
						setPosY(getPosY() - 1);

						Thread.sleep(5);
						
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		hilo.start();
	}
}
