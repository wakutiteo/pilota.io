import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;

public class MiRanking {

	private int size;
	private int posX, posY;
	private Circulo circulo;
	private String cadenaLogitud;


	public MiRanking(Circulo circulo) {
		this.circulo = circulo;

		size = circulo.getSize();
		posX = 10;
		posY = CanvasJuego.ALTO - 10;
		cadenaLogitud = "Tu tamaño:  ";
	}

	
	// Métodos
	public void dibujar(Graphics2D g2) {
		// Actualizamos el Ranking
		size = circulo.getSize();

		// Dibujar
		g2.setFont(Pilotaio.FUENTE_RANKING);
		FontMetrics fm = g2.getFontMetrics();

		g2.setColor(new Color(1, 1, 1, 0.3f));
		g2.drawString(cadenaLogitud, posX, posY);
		g2.setColor(new Color(1, 1, 1, 1f));
		g2.drawString(String.valueOf(size), posX +  fm.stringWidth(cadenaLogitud), posY);

		g2.setFont(Pilotaio.FUENTE);
	}
}
